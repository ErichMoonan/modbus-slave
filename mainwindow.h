#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QModbusServer>
#include <QButtonGroup>
#include <QLineEdit>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonConnect_clicked();

    void on_pushButtonDisconnect_clicked();

    void coilChanged(int id);

    void discreteInputChanged(int id);

    void setInputRegister(const QString &value);

    void setHoldingRegister(const QString &value);

    void UpdateDisplayBySlave(QModbusDataUnit::RegisterType table, int address, int size);

    void HandleDeviceError(QModbusDevice::Error newError);

    void SlaveStateChanged(int state);

private:
    void SearchSerialPorts();

    void InitialSetting();

    void InitialInterfaceariables();

    void InitialSlaveDatas();

private:
    Ui::MainWindow *ui;

    QModbusServer *modbusDevice = nullptr;

    QButtonGroup coilButtons;
    QButtonGroup discreteButtons;
    QHash<QString, QLineEdit *> inputRegisters;
    QHash<QString, QLineEdit *> holdingRegisters;
};
#endif // MAINWINDOW_H
